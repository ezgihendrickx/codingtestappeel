# codingTestAppeel

An app that connects to the Github API, and lists all public repositories from an user, and the last commits for the repositories.

# Vue.js: [Live](https://clever-davinci-f6420b.netlify.app/#/)

* I wanted to challenge myself therefore I have chosen vue.js I haven’t worked with vue.js before. It was an opportunity for me to add a new skills to my toolbox. 

- Vue.js is also approachable, versatile, performant, maintainable and testable. 
- Vue is a progressive framework which means more richer and interactieve experience. 
- Vue is reactive when the data changes then html is also change.

* I have chosen to use Bulma CSS framework. Because I have gained already experiences with Tailwind and Bootstrap. 

* I used Postman API platform. Because it is easy to test.  Ref.  https://api.github.com

1. I clicked reference in the documentation page of Github API and then I clicked the repositories after that list repositories for a user. 

https://api.github.com/users/ezgihendrickx/repos I wrote this link in the search bar of the Postman. I had response of all my repositories in a JSON file. 

This point I needed to add axios(library) to my vue.js app.(for my data)

- To install axios  ```npm install axios```
In a nutshell, Axios is a Javascript library used to make HTTP requests from node.js or XMLHttpRequests from the browser that also supports the ES6 Promise API. 
In the end I connected successfully the Github API with Rest API and I had all dynamic lists of GitHub repositories. 

2. For the second challenge I had to add vue router and I found out it is easier in the Vue GUI.

I typed in my terminal `vue ui` it opens automatically my UI panel. Later I clicked plugins and added the extension of Vue router. 


3. I added router to connect to another page and list my GitHub repo commits. I have checked list repo commits in the GitHub Api docs. 

https://api.github.com/repos/ezgihendrickx/gatsby-workshop/commits

/repos/{owner}/{repo}/commits  I wrote this link in the search bar of the Postman. I had response of ‘gatsby-workshop’ repository commits in a JSON file. 

I have listed all commits of that repository. But according to the test I need to show only last 20 commits. Therefore I checked online, and I have found solution in stackoverflow here Ref: https://stackoverflow.com/questions/15919635/on-github-api-what-is-the-best-way-to-get-the-last-commit-message-associated-w 

In this case my link converted to this api link: https://api.github.com/repos/ezgihendrickx/gatsby-workshop/commits?page=1&per_page=20 


 4. Be able to create custom search filter I needed to create a computed attribute in the vue.js commits file. 



